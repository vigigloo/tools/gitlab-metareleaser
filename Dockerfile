ARG BASE_IMAGE=alpine:3.16.0
FROM $BASE_IMAGE
ARG DASEL_VERSION=1.24.3
ARG ARCH=amd64

ADD gitlab-metareleaser.sh /usr/local/bin/gitlab-metareleaser.sh
RUN chmod +x /usr/local/bin/gitlab-metareleaser.sh

RUN apk add curl bash tar bzip2
RUN curl -Lo dasel https://github.com/TomWright/dasel/releases/download/v${DASEL_VERSION}/dasel_linux_${ARCH} && \
    chmod +x ./dasel && \
    mv ./dasel /usr/local/bin/dasel

CMD ["gitlab-metareleaser.sh"]
