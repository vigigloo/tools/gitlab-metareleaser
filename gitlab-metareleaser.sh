#!/usr/bin/env bash
CONFIG_PATH=./metareleaser.toml
OUT=${1:-out}
SELECTOR=${2:-.}
NAMESPACE=$3

if [ ! -f $CONFIG_PATH ]; then
    echo "$CONFIG_PATH not found";
    exit 1;
fi

function getUriFilename() {
    header="$(curl -sI "$1" | tr -d '\r')"

    filename="$(echo "$header" | grep -o -E 'filename=.*$')"
    if [[ -n "$filename" ]]; then
        echo "${filename#filename=}"
        return
    fi

    filename="$(echo "$header" | grep -o -E 'Location:.*$')"
    if [[ -n "$filename" ]]; then
        basename "${filename#Location\:}"
        return
    fi

    basename $1
    return
}

mkdir -p $OUT

for i in $(dasel -f $CONFIG_PATH -m "$SELECTOR-"); do
  case $i in
  copy)
    for j in $(dasel -f $CONFIG_PATH -m "${SELECTOR}copy.[*]"); do
      echo "Copying content of $j to $OUT..."
      if [ -d $j ];
      then
          cp -r $j "$OUT/"
      else
          cp $j "$OUT/"
      fi
    done
    ;;
  get)
    for j in $(dasel -f $CONFIG_PATH -m "${SELECTOR}get.[*]"); do
      echo "Fetching $j into $OUT..."
      (cd $OUT && curl -sO $j)
    done
    ;;
  uncompress)
    CUR_PATH=`pwd`
    mkdir -p /tmp/metareleaser/$NAMESPACE && cd $_
    for j in $(dasel -f "$CUR_PATH/$CONFIG_PATH" -m "${SELECTOR}uncompress.[*]"); do
      url=$j
      url=$(sed "s|\$CI_COMMIT_TAG|$CI_COMMIT_TAG|g" <<< $url)
      url=$(sed "s|\$TAG|$TAG|g" <<< $url)
      url=$(sed "s|\$CI_COMMIT_SHORT_SHA|$CI_COMMIT_SHORT_SHA|g" <<< $url)
      url=$(sed "s|\$CI_COMMIT_SHA|$CI_COMMIT_SHA|g" <<< $url)
      filename=$(getUriFilename $url)
      curl -so $filename $url
      echo "Fetching $url into temporary folder..."
      case "$filename" in
        *.tar.bz2 )
          echo "Untar $filename into $OUT..."
          tar -xjf $filename --directory "$CUR_PATH/$OUT/"
          ;;
        *)
          echo "Not supported extension to uncompress"
          ;;
      esac
    done
    rm -rf /tmp/metareleaser/$NAMESPACE
    cd $CUR_PATH
    ;;
  *)
    $0 "$OUT/$i" "$SELECTOR$i." $i
    ;;
  esac
done
